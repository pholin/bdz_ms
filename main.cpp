#include <iostream>
#include <string>
#include <fstream>
#include <cstdio>
#include <sstream>
#include <cmath>
#include <functional>
#include <array>
#include <utility>

#include <boost/math/distributions.hpp>
#include <boost/accumulators/statistics/variance.hpp>
#include <boost/accumulators/accumulators.hpp>
#include <boost/accumulators/statistics/stats.hpp>
#include <boost/accumulators/statistics/mean.hpp>
#include <boost/accumulators/statistics/moment.hpp>
#include <boost/accumulators/statistics/max.hpp>
#include <boost/accumulators/statistics/min.hpp>
#include <boost/accumulators/statistics/kurtosis.hpp>
#include <boost/accumulators/statistics/skewness.hpp>

#include <boost/numeric/ublas/matrix.hpp>
#include <boost/numeric/ublas/io.hpp>
#include <boost/numeric/ublas/lu.hpp>

using namespace boost::accumulators;
using namespace boost::math;
using namespace boost::numeric::ublas;

using DataVec = std::vector< double >;
using DataAcc = boost::accumulators::accumulator_set<
    double,
    stats<
        tag::variance(lazy),
        tag::min,
        tag::max,
        tag::skewness,
        tag::kurtosis> >;

using QualityData = std::vector< std::string >;

struct DataInfo
{
    DataInfo(const DataVec &_data)
    {
        DataAcc data;
        for (auto const &val : _data)
            data(val);

        N        = count(data);
        mean     = boost::accumulators::mean(data);
        S2       = ((boost::accumulators::variance(data) * N) / (N - 1));
        S        = sqrt(S2);
        min      = boost::accumulators::min(data);
        max      = boost::accumulators::max(data);
        kurtosis = boost::accumulators::kurtosis(data);
        skewness = boost::accumulators::skewness(data);
    }

    int N;
    double mean     ,
           S        ,
           S2       ,
           min      ,
           max      ,
           kurtosis ,
           skewness ;
};

int intervalSturges(const DataVec &data);
int intervalScott  (const DataVec &data);

std::function< int(const DataVec &) > intervalCalculator = intervalSturges;
bool printGraphData = false;

int intervalSturges(const DataVec &data)
{
    return 1 + 1.3 * std::log10(data.size());
}

int intervalScott(const DataVec &data)
{
    DataInfo info(data);

    double width = 3.5 * info.S * std::pow(info.N, -1.0 / 3);
    return (info.max - info.min) / width;
}

double sqr(double z) { return z*z; }

void declareSection(const std::string &name)
{
    std::cout << "\n*==========*   " << name << "   *==========*\n\n";
}

void calc1_2(const DataVec &data)
{
    declareSection("#1.2");

    DataInfo info(data);

    int groupNum = intervalCalculator(data);

    printf("Group num: %i\n", groupNum);
    double intervalWidth = (info.max - info.min) / groupNum;
    printf("Interval width: %f\n\n", intervalWidth);


    std::vector<double> v_freq,
                        v_relFreq,
                        v_sumFreq,
                        v_sumRelFreq;
    printf("#\tlowerBound\tupperBound\tFreq\tRelFreq\tSumFreq\tSumRelFreq\n");
    int    sumFreq    = 0;
    double sumRelFreq = 0;
    for (int i = 0; i < groupNum; ++i)
    {
        const double lw = info.min +  i    * intervalWidth;
        const double up = info.min + (i+1) * intervalWidth + ((i == (groupNum - 1)) ? 0.00001 : 0.0);
        int count       = 0;

        for (auto const &v: data)
        {
            if (v >= lw && v < up) ++count;
        }

        double relFreq = ((double) count) / info.N;
        sumFreq += count;
        sumRelFreq += relFreq;

        v_freq.push_back(count);
        v_relFreq.push_back(relFreq);
        v_sumFreq.push_back(sumFreq);
        v_sumRelFreq.push_back(sumRelFreq);

        std::cout << i << '\t' << lw << '\t' << up << '\t' << count << '\t' << relFreq << '\t' << sumFreq << '\t' << sumRelFreq << std::endl;
    }
    if (printGraphData)
    {
        std::cout << "{{{ Graph data }}}" << std::endl;
        std::cout << "Freq" << std::endl;
        for (int i = 0; i < groupNum; ++i)
        {
            const double lw = info.min +  i    * intervalWidth;
            const double up = info.min + (i+1) * intervalWidth + ((i == (groupNum - 1)) ? 0.00001 : 0.0);
            std::cout << (lw + up) / 2 << ' ' << v_freq[i] << std::endl;

        }
        std::cout << std::endl;

        std::cout << "RelFreq" << std::endl;
        for (int i = 0; i < groupNum; ++i)
        {
            const double lw = info.min +  i    * intervalWidth;
            const double up = info.min + (i+1) * intervalWidth + ((i == (groupNum - 1)) ? 0.00001 : 0.0);
            std::cout << (lw + up) / 2 << ' ' << v_relFreq[i] << std::endl;
        }
        std::cout << std::endl;

        std::cout << "SumFreq" << std::endl;
        for (int i = 0; i < groupNum; ++i)
        {
            const double lw = info.min +  i    * intervalWidth;
            const double up = info.min + (i+1) * intervalWidth + ((i == (groupNum - 1)) ? 0.00001 : 0.0);
            std::cout << (lw + up) / 2 << ' ' << v_sumFreq[i] << std::endl;
        }
        std::cout << std::endl;

        std::cout << "SumRelFreq" << std::endl;
        for (int i = 0; i < groupNum; ++i)
        {
            const double lw = info.min +  i    * intervalWidth;
            const double up = info.min + (i+1) * intervalWidth + ((i == (groupNum - 1)) ? 0.00001 : 0.0);
            std::cout << (lw + up) / 2 << ' ' << v_sumRelFreq[i] << std::endl;
        }
        std::cout << std::endl;

        std::cout << "Empirical function:" << std::endl;

        auto sdata = data;
        std::sort(sdata.begin(), sdata.end());

        for (int i = 0; i < sdata.size(); ++i)
        {
            std::cout << sdata[i] << ' ' << ((double) i) / sdata.size() << std::endl;
        }
        std::cout << sdata.back() + 1 << ' ' << sdata.size() << std::endl;
    }
}

void calc2_1(const DataVec &data)
{
    declareSection("#2.1");
    DataInfo info(data);

    std::vector<double> alphas;
    alphas.push_back(0.01);
    alphas.push_back(0.05);
    alphas.push_back(0.1);

    students_t dist(info.N-1);

    for (double alp: alphas)
    {
        double lowerBound = info.mean - (info.S / std::sqrt(info.N)) * (boost::math::quantile(dist, 1 - alp/2));
        double upperBound = info.mean + (info.S / std::sqrt(info.N)) * (boost::math::quantile(dist, 1 - alp/2));
        printf("Alpha = %f; lower = %f; upper = %f\n", alp, lowerBound, upperBound);
    }
}

void calc2_2(const DataVec &data)
{
    declareSection("#2.2");
    DataInfo info(data);

    chi_squared dist(info.N - 1);

    std::vector<double> alphas;
    alphas.push_back(0.01);
    alphas.push_back(0.05);
    alphas.push_back(0.1);

    for (double alp: alphas)
    {
        double lowerBound = (info.N - 1) * info.S2 / (boost::math::quantile(dist, 1 - alp/2));
        double upperBound = (info.N - 1) * info.S2 / (boost::math::quantile(dist,     alp/2));
        printf("Alpha = %f; lower = %f; upper = %f\n", alp, lowerBound, upperBound);
    }
}

void calc2_3(const DataVec &data1, const DataVec &data2)
{
    declareSection("#2.3");
    DataInfo info1(data1),
               info2(data2);

    std::vector<double> alphas;
    alphas.push_back(0.01);
    alphas.push_back(0.05);
    alphas.push_back(0.1);

    students_t dist(info1.N + info2.N - 2);

    for (double alp: alphas)
    {
        double lowerBound = (info1.mean - info2.mean)
                + boost::math::quantile(dist, alp/2)
                * std::sqrt(1.0 / info1.N + 1.0 / info2.N)
                * std::sqrt(((info1.N - 1) * info1.S + (info2.N - 1) * info2.S) / (info1.N + info2.N - 2));
        double upperBound = (info1.mean - info2.mean)
                - boost::math::quantile(dist, alp/2)
                * std::sqrt(1.0 / info1.N + 1.0 / info2.N)
                * std::sqrt(((info1.N - 1) * info1.S + (info2.N - 1) * info2.S) / (info1.N + info2.N - 2));

        printf("Alpha = %f; lower = %f; upper = %f\n", alp, lowerBound, upperBound);
    }
}

void calc2_4(const DataVec &data1, const DataVec &data2)
{
    declareSection("#2.4");
    DataInfo info1(data1),
               info2(data2);

    std::vector<double> alphas;
    alphas.push_back(0.01);
    alphas.push_back(0.05);
    alphas.push_back(0.1);

    fisher_f dist(info2.N - 1, info1.N - 1);

    for (double alp: alphas)
    {
        double lowerBound = boost::math::quantile(dist,     alp/2) * info1.S2 / info2.S2;
        double upperBound = boost::math::quantile(dist, 1 - alp/2) * info1.S2 / info2.S2;

        printf("Alpha = %f; lower = %f; upper = %f\n", alp, lowerBound, upperBound);
    }
}

void calc3_1(const DataVec &data, double m0)
{
    declareSection("#3.1");
    DataInfo info(data);

    double alpha = 0.1;

    students_t dist(info.N-1);

//    double Zleft  = boost::math::quantile(dist,     alpha/2);
//    double Zright = boost::math::quantile(dist, 1 - alpha/2);

    double Zsample = std::sqrt(info.N) * (info.mean - m0) / info.S;

    double pval = 2 * std::min(cdf(dist, Zsample), 1 - cdf(dist, Zsample));

    printf("m0 = %f; alpha = %f; Zsample = %f; pvalue = %f; H0 is %s\n", m0, alpha, Zsample, pval, (pval > alpha) ? "true" : "false");
}

void calc3_2(const DataVec &data, double sigma0)
{
    declareSection("#3.2");
    DataInfo info(data);

    double alpha = 0.1;

    chi_squared dist(info.N-1);

    double Zsample = (info.N - 1) * info.S2 / (sigma0*sigma0);

    double pval = 2 * std::min(cdf(dist, Zsample), 1 - cdf(dist, Zsample));

    printf("sigma0 = %f; alpha = %f; Zsample = %f; pvalue = %f; H0 is %s\n", sigma0, alpha, Zsample, pval, (pval > alpha) ? "true" : "false");
}

void calc3_3(const DataVec &data1, const DataVec &data2)
{
    declareSection("#3.3");
    DataInfo info1(data1),
             info2(data2);


    std::vector<double> alphas;
    alphas.push_back(0.01);
    alphas.push_back(0.05);
    alphas.push_back(0.1);

    double S2 = ((info1.N - 1) * info1.S2 + (info2.N - 1) * info2.S2) / (info1.N + info2.N - 2);

    students_t dist(info1.N + info2.N - 1);

    double Zsample = (info1.mean - info2.mean) / (std::sqrt(S2 * (1.0 / info1.N + 1.0 / info2.N)));

    double pval = 2 * std::min(cdf(dist, Zsample), 1 - cdf(dist, Zsample));


    for (double alp: alphas)
    {
        printf("alpha = %f; Zsample = %f; pvalue = %f; H0 is %s\n", alp, Zsample, pval, (pval > alp) ? "true" : "false");
    }
}

void calc3_4(const DataVec &data1, const DataVec &data2)
{
    declareSection("#3.4");
    DataInfo info1(data1),
               info2(data2);

    std::vector<double> alphas;
    alphas.push_back(0.01);
    alphas.push_back(0.05);
    alphas.push_back(0.1);

    fisher_f dist(info1.N - 1, info2.N - 1);

    double Zsample = info1.S2 / info2.S2;

    double pval = 2 * std::min(cdf(dist, Zsample), 1 - cdf(dist, Zsample));

    for (double alp: alphas)
    {
        printf("alpha = %f; Zsample = %f; pvalue = %f; H0 is %s\n", alp, Zsample, pval, (pval > alp) ? "true" : "false");
    }
}

void calc4_1(const DataVec &data)
{
    declareSection("#4.1");
    DataInfo info(data);

    std::vector<double> alphas;
    alphas.push_back(0.01);
    alphas.push_back(0.05);
    alphas.push_back(0.1);

    int groupNum = intervalCalculator(data);
    if (groupNum < 4)
        groupNum = 4;

    printf("Group num: %i\n", groupNum);
    double intervalWidth = (info.max - info.min) / groupNum;
    printf("Interval width: %f\n\n", intervalWidth);

    normal norm(info.mean, info.S);

    double Zsample = 0;


    std::vector< double > v_relFreq;
    printf("#\tlowerBound\tupperBound\tFreq\tRelFreq\tProb|H0\n");
    for (int i = 0; i < groupNum; ++i)
    {
        const double lw = info.min +  i    * intervalWidth;
        const double up = info.min + (i+1) * intervalWidth + ((i == (groupNum - 1)) ? 0.00001 : 0.0);
        int count       = 0;

        for (auto const &v: data)
        {
            if (v >= lw && v < up) ++count;
        }

        double relFreq = ((double) count) / info.N;
        double prob    = cdf(norm, up) - cdf(norm, lw);

        v_relFreq.push_back(relFreq);

        printf("%i\t%f\t%f\t%i\t%f\t%f\n",
               i+1,
               lw,
               up,
               count,
               relFreq,
               prob
               );

        if (count)
        {
            Zsample += sqr(count - info.N * prob) / (info.N * prob);
        }
        else
        {
            Zsample += info.N * prob;
        }
    }
    std::cout << std::endl;

    chi_squared dist(groupNum - 2 - 1);

    double pval;

    if (std::isinf(Zsample))
    {
        pval = 0;
    }
    else
    {
        pval = 2 * std::min(cdf(dist, Zsample), 1 - cdf(dist, Zsample));
    }

    for (double alp: alphas)
    {
        printf("alpha = %f; Zsample = %f; pvalue = %f; H0 is %s\n", alp, Zsample, pval, (pval > alp) ? "true" : "false");
    }

    if (printGraphData)
    {
        std::cout << "{{{ Graph data }}}" << std::endl;
        std::cout << "RelFreq" << std::endl;
        for (int i = 0; i < groupNum; ++i)
        {
            const double lw = info.min +  i    * intervalWidth;
            const double up = info.min + (i+1) * intervalWidth + ((i == (groupNum - 1)) ? 0.00001 : 0.0);
            std::cout << (lw + up) / 2 << ' ' << v_relFreq[i] << std::endl;
        }
        std::cout << std::endl;

        std::cout << "Normal(m0, s0) ~ " << info.mean << ' ' << info.S << std::endl;

        std::cout << std::endl;
    }

}

void calc4_2(const DataVec &data)
{
    declareSection("#4.2");
    DataInfo info(data);

    std::vector<double> alphas;
    alphas.push_back(0.01);
    alphas.push_back(0.05);
    alphas.push_back(0.1);

    normal dist(0, 1);

    double ZsampleKurt = info.kurtosis / std::sqrt(24.0 / info.N); // эксцесс
    double ZsampleSkew = info.skewness / std::sqrt(6.0  / info.N); // асимметрия

    double pvalKurt = 2 * std::min(cdf(dist, ZsampleKurt), 1 - cdf(dist, ZsampleKurt));
    double pvalSkew = 2 * std::min(cdf(dist, ZsampleSkew), 1 - cdf(dist, ZsampleSkew));

    printf("Alpha\t\ttype\tZsample\t\tpvalue\t\tResult\n");
    for (double alp: alphas)
    {
        printf("%f\t%s\t%f\t%f\tH0_Kurt is %s\n", alp, "Kurt", ZsampleKurt, pvalKurt, (pvalKurt > alp) ? "true" : "false");
        printf("%f\t%s\t%f\t%f\tH0_Skew is %s\n", alp, "Skew", ZsampleSkew, pvalSkew, (pvalSkew > alp) ? "true" : "false");
        printf("H0 is %s\n", ((pvalKurt > alp) && (pvalSkew > alp)) ? "don't know()" : "false");
    }
}

void calc5_1(const DataVec &data1, const DataVec &data2)
{
    declareSection("#5.1");
    DataInfo info1(data1),
               info2(data2);

    std::vector<double> alphas;
    alphas.push_back(0.01);
    alphas.push_back(0.05);
    alphas.push_back(0.1);

    int Kplus = 0;
    const int sz = std::min(info1.N, info2.N);
    for (int i = 0; i < sz; ++i)
    {
        if ((data1[i] - data2[i]) > 0) ++Kplus;
    }

    double Zsample = (Kplus - sz/2.0) / (std::sqrt(sz) / 2);

    normal dist(0, 1);

    double pval = 2 * std::min(cdf(dist, Zsample), 1 - cdf(dist, Zsample));

    for (double alp: alphas)
    {
        printf("alpha = %f; Zsample = %f; pvalue = %f; H0 is %s\n", alp, Zsample, pval, (pval > alp) ? "true" : "false");
    }
}

void calc5_2(const DataVec &data1, const DataVec &data2)
{
    declareSection("#5.2");
    DataInfo info1(data1),
             info2(data2);

    std::vector<double> alphas;
    alphas.push_back(0.01);
    alphas.push_back(0.05);
    alphas.push_back(0.1);

    int groupNum = (intervalCalculator(data1) + intervalCalculator(data2)) / 2;
    printf("Group num: %i\n", groupNum);
    double intervalMin = std::min(info1.min, info2.min);
    double intervalWidth = (std::max(info1.max, info2.max) - intervalMin) / groupNum;
    printf("Interval width: %f\n\n", intervalWidth);

    double Zsample = 0;

    std::vector< double > v_relFreq1,
                          v_relFreq2;

    printf("#\tlowerBound\tupperBound\tFreq1\tFreq2\tRelFreq1\tRelFreq2\n");
    for (int i = 0; i < groupNum; ++i)
    {
        const double lw = intervalMin +  i    * intervalWidth;
        const double up = intervalMin + (i+1) * intervalWidth + ((i == (groupNum - 1)) ? 0.00001 : 0.0);
        int count1      = 0;
        int count2      = 0;

        for (auto const &v: data1) { if (v >= lw && v < up) ++count1; }
        for (auto const &v: data2) { if (v >= lw && v < up) ++count2; }

        double relFreq1 = ((double) count1) / info1.N;
        double relFreq2 = ((double) count2) / info2.N;

        v_relFreq1.push_back(relFreq1);
        v_relFreq2.push_back(relFreq2);

        if ((count1 + count2) != 0)
            Zsample += (relFreq1 - relFreq2) * (relFreq1 - relFreq2) / (count1 + count2);

        printf("%i\t%f\t%f\t%i\t%i\t%f\t%f\n",
               i + 1,
               lw,
               up,
               count1,
               count2,
               relFreq1,
               relFreq2);
    }
    Zsample *= info1.N * info2.N;

    std::cout << std::endl;

    chi_squared dist(groupNum - 1);

    double pval = 1 - cdf(dist, Zsample);

    for (double alp: alphas)
    {
        printf("alpha = %f; Zsample = %f; pvalue = %f; H0 is %s\n", alp, Zsample, pval, (pval > alp) ? "true" : "false");
    }

    if (printGraphData)
    {
        std::cout << std::endl;
        std::cout << "{{{ Graph data }}}" << std::endl;
        std::cout << "RelFreq1" << std::endl;
        for (int i = 0; i < groupNum; ++i)
        {
            const double lw = intervalMin +  i    * intervalWidth;
            const double up = intervalMin + (i+1) * intervalWidth + ((i == (groupNum - 1)) ? 0.00001 : 0.0);
            std::cout << (lw + up) / 2 << ' ' << v_relFreq1[i] << std::endl;
        }
        std::cout << std::endl;

        std::cout << "RelFreq2" << std::endl;
        for (int i = 0; i < groupNum; ++i)
        {
            const double lw = intervalMin +  i    * intervalWidth;
            const double up = intervalMin + (i+1) * intervalWidth + ((i == (groupNum - 1)) ? 0.00001 : 0.0);
            std::cout << (lw + up) / 2 << ' ' << v_relFreq2[i] << std::endl;
        }
        std::cout << std::endl;
    }
}

int calcQualityVariants(const QualityData &data, QualityData &variants)
{
    variants.push_back(data.front());
    int res = 1;
    for (const auto &s: data)
    {
        bool in = false;
        for (const auto &v: variants)
        {
            if (s == v)
            {
                in = true;
                break;
            }
        }
        if (! in)
        {
            ++res;
            variants.push_back(s);
        }
    }
    return res;
}

void calc6_1(const QualityData &data1, const QualityData &data2)
{
    declareSection("#6.1");

    // 1 -- fact, 2 -- res
    if (data1.size() != data2.size())
    {
        printf("Invalid sample sizes: %zu %zu\n", data1.size(), data2.size());
    }
    const int N = data2.size();


    QualityData vars1, vars2;
    const int K1 = calcQualityVariants(data1, vars1);
    const int K2 = calcQualityVariants(data2, vars2);

    std::vector< std::vector< int > > realFreq(K1 + 1, std::vector< int >(K2 + 1, 0));
    for (int i = 0; i < K1; ++i)
    {
        for (int j = 0; j < K2; ++j)
        {
            for (int k = 0; k < N; ++k)
            {
                if (data1[k] == vars1[i] && data2[k] == vars2[j])
                    ++realFreq[i][j];
            }
        }
    }

    for (int i = 0; i < K1; ++i)
    {
        int sum = 0;
        for (int j = 0; j < K2; ++j)
        {
            sum += realFreq[i][j];
        }
        realFreq[i][K2] = sum;
    }

    for (int i = 0; i <= K2; ++i)
    {
        int sum = 0;
        for (int j = 0; j < K1; ++j)
        {
            sum += realFreq[j][i];
        }
        realFreq[K1][i] = sum;
    }


    printf("Empirical table:\n");
    printf("x\\y\t");
    for (int i = 0; i < K1; ++i)
    {
        printf("%s\t", vars1[i].c_str());
    }
    printf("sum\n");

    for (int i = 0; i < K2; ++i)
    {
        printf("%s\t", vars2[i].c_str());
        for (int j = 0; j < K1 + 1; ++j)
        {
            printf("%i\t", realFreq[j][i]);
        }
        printf("\n");
    }
    printf("sum\t");
    for (int j = 0; j < K1 + 1; ++j)
    {
        printf("%i\t", realFreq[j][K2]);
    }
    printf("\n\n");

    std::vector< std::vector< double > > theorFreq(K1 + 1, std::vector< double >(K2 + 1, 0));

    for (int i = 0; i < K1; ++i)
    {
        for (int j = 0; j < K2; ++j)
        {
            for (int k = 0; k < N; ++k)
            {
                theorFreq[i][j] = (((double) realFreq[K1][j]) / N) * realFreq[i][K2];
            }
        }
    }

    for (int i = 0; i < K1; ++i)
    {
        int sum = 0;
        for (int j = 0; j < K2; ++j)
        {
            sum += theorFreq[i][j];
        }
        theorFreq[i][K2] = sum;
    }

    for (int i = 0; i <= K2; ++i)
    {
        int sum = 0;
        for (int j = 0; j < K1; ++j)
        {
            sum += theorFreq[j][i];
        }
        theorFreq[K1][i] = sum;
    }

    printf("Theoretical table:\n");
    printf("x\\y\t");
    for (int i = 0; i < K1; ++i)
    {
        printf("%s\t", vars1[i].c_str());
    }
    printf("sum\n");

    for (int i = 0; i < K2; ++i)
    {
        printf("%s\t", vars2[i].c_str());
        for (int j = 0; j < K1 + 1; ++j)
        {
            printf("%f\t", theorFreq[j][i]);
        }
        printf("\n");
    }
    printf("sum\t");
    for (int j = 0; j < K1 + 1; ++j)
    {
        printf("%f\t", theorFreq[j][K2]);
    }
    printf("\n\n");

    double Zsample = 0;

    for (int i = 0; i < K1; ++i)
    {
        for (int j = 0; j < K2; ++j)
        {
            Zsample += (realFreq[i][j] - theorFreq[i][j]) * (realFreq[i][j] - theorFreq[i][j]) / theorFreq[i][j];
        }
    }

    chi_squared dist((K1 - 1) * (K2 - 1));

    double pval = 1 - cdf(dist, Zsample);

    std::vector<double> alphas;
    alphas.push_back(0.01);
    alphas.push_back(0.05);
    alphas.push_back(0.1);

    for (double alp: alphas)
    {
        printf("alpha = %f; Zsample = %f; pvalue = %f; H0 is %s\n", alp, Zsample, pval, (pval > alp) ? "true" : "false");
    }
}

void calc7_1(const QualityData &data1, const DataVec &data2)
{
    declareSection("#7.1");

    if (data1.size() != data2.size())
    {
        printf("Invalid sample sizes: %zu %zu\n", data1.size(), data2.size());
    }
    const int N = data2.size();

    QualityData vars1;
    const int K = calcQualityVariants(data1, vars1);
    std::vector< DataInfo > infos;
    DataInfo infoGen(data2);
    for (const auto &v: vars1)
    {
        DataVec tmp;

        for (int i = 0; i < N; ++i)
        {
            if (data1[i] == v)
                tmp.push_back(data2[i]);
        }

        infos.push_back(DataInfo(tmp));
    }

    printf("#\tVar\tSize\tGroupMean\tGroupVariance\n");
    for (int i = 0; i < K; ++i)
    {
        printf("%i\t%s\t%i\t%f\t%f\n",
               i + 1,
               vars1[i].c_str(),
               infos[i].N,
               infos[i].mean,
               infos[i].S2);
    }
    std::cout << std::endl;

    double Dgroup(0), Dinner(0), Dgeneral(0);

    for (const auto &inf: infos)
    {
        Dgroup += (inf.N * sqr(inf.mean - infoGen.mean)) / infoGen.N;
        Dinner += (inf.N * inf.S2 * (inf.N - 1) / (inf.N)) / infoGen.N;
    }

    for (int i = 0; i < infoGen.N; ++i)
    {
        Dgeneral += sqr(data2[i] - infoGen.mean) / N;
    }

    double DgeneralUnbiased = N * Dgeneral / (N - 1),
           DgroupUnbiased   = N * Dgroup   / (K - 1),
           DinnerUnbiased   = N * Dinner   / (N - K);

    std::cout << "Dgroup   = " << Dgroup   << "; DgroupUnbiased   = " << DgroupUnbiased   << std::endl;
    std::cout << "Dinner   = " << Dinner   << "; DinnerUnbiased   = " << DinnerUnbiased   << std::endl;
    std::cout << "Dgeneral = " << Dgeneral << "; DgeneralUnbiased = " << DgeneralUnbiased << std::endl;
    std::cout << std::setw(10) << "Dgroup" << '\t' << std::setw(10) << "Dinner" << '\t' << std::setw(10) << "Dgenereal" << '\t' << std::setw(10) << "Dgroup + Dinner\n";
    std::cout << std::setw(10) <<  Dgroup  << '\t' << std::setw(10) <<  Dinner  << '\t' << std::setw(10) <<  Dgeneral   << '\t' << std::setw(10) << Dgroup + Dinner << std::endl;
    std::cout << std::endl;

    double empDetermFactor = Dgroup / Dgeneral;
    double empCorrelRatio  = std::sqrt(empDetermFactor);

    std::cout << "Empirical determination factor = " << empDetermFactor << std::endl;
    std::cout << "Empirical correlation   ratio  = " << empCorrelRatio  << std::endl;
    std::cout << std::endl;

    double Zsample = DgroupUnbiased / DinnerUnbiased;

    fisher_f dist(K - 1, N - K);

    double pval = 1 - cdf(dist, Zsample);

    std::vector<double> alphas;
    alphas.push_back(0.01);
    alphas.push_back(0.05);
    alphas.push_back(0.1);

    for (double alp: alphas)
    {
        printf("alpha = %f; Zsample = %f; pvalue = %f; H0 is %s\n", alp, Zsample, pval, (pval > alp) ? "true" : "false");
    }
}

void calcRanks(const DataVec &table, DataVec &ranks)
{
    const int N = table.size();
    std::vector< int > result;
    result.resize(N);
    ranks.resize(N);
    for(int i = 0; i < N; i++)
    {
        int rnk = 1;
        for (int z = 0; z < N; z++)
        {
            if (table[z] < table[i])
                rnk++;
        }
        result[i] = rnk;
    }

    for (int i = 1; i <= N; ++i)
    {
        int rankNum = 0;
        for (int j = 0; j < N; ++j)
        {
            if (result[j] == i)
                ++rankNum;
        }

        double realRank = (2 * i + rankNum - 1) / 2.0;

        for (int j = 0; j < N; ++j)
        {
            if (result[j] == i)
                ranks[j] = realRank;
        }
    }
}

double calcLinearCorrelFactor(const DataVec &data1, const DataVec &data2, double mean1, double mean2, double sigma1, double sigma2)
{
    double linearCorrelFactor = 0;
    for (size_t i = 0; i < data1.size(); ++i)
    {
        linearCorrelFactor += (data1[i] - mean1) * (data2[i] - mean2) / data1.size();
    }
    linearCorrelFactor /= sigma1 * sigma2;

    return linearCorrelFactor;
}

void calc8_1(const DataVec &data1, const DataVec &data2)
{
    declareSection("#8.1");

    if (data1.size() != data2.size())
    {
        printf("Invalid sample sizes: %zu %zu\n", data1.size(), data2.size());
    }
    const int N = data2.size();

    DataInfo info1(data1),
             info2(data2);

    const double linearCorrelFactor = calcLinearCorrelFactor(
                data1,
                data2,
                info1.mean,
                info2.mean,
                info1.S,
                info2.S);



    DataVec ranks1, ranks2;
    calcRanks(data1, ranks1);
    calcRanks(data2, ranks2);

    DataInfo rankInfo1(ranks1),
             rankInfo2(ranks2);

    const double SpearmanCorrelFactor = calcLinearCorrelFactor(
                ranks1,
                ranks2,
                rankInfo1.mean,
                rankInfo2.mean,
                rankInfo1.S,
                rankInfo2.S);

    std::vector < std::pair< double, double > > rankPairs;
    for (int i = 0; i < N; ++i)
    {
        rankPairs.emplace_back(ranks1[i], ranks2[i]);
    }
    std::sort(rankPairs.begin(), rankPairs.end(),
        [] (const auto &a, const auto &b) -> bool
        {
            return a.first < b.first;
        }
    );
    int R = 0;
    for (int i = 0; i < N; ++i)
    {
        for (int j = i + 1; j < N; ++j)
        {
            if (rankPairs[i].second > rankPairs[j].second)
                ++R;
        }
    }
    double KendallCorrelFactor = 4.0 * R / (N * (N - 1)) - 1;

    std::cout << "Linear   correlation factor = " << linearCorrelFactor   << std::endl;
    std::cout << "Spearman correlation factor = " << SpearmanCorrelFactor << std::endl;
    std::cout << "Kendall  correlation factor = " << KendallCorrelFactor  << std::endl;
    std::cout << std::endl;

    std::vector<double> alphas;
    alphas.push_back(0.01);
    alphas.push_back(0.05);
    alphas.push_back(0.1);

    std::cout << std::setw(10) << "Alpha" << '\t' << std::setw(10) << "lower" << '\t' << std::setw(10) << "Upper" << "\n";
    for (double alp: alphas)
    {
        normal dist(0, 1);
        double lw, up;
        if (N > 500)
        {
            lw = linearCorrelFactor +
                 linearCorrelFactor * (1 - linearCorrelFactor * linearCorrelFactor) / (2.0 * N)
                 - boost::math::quantile(dist, 1 - alp/2) * (1 - linearCorrelFactor * linearCorrelFactor) / std::sqrt(N);
            up = linearCorrelFactor +
                 linearCorrelFactor * (1 - linearCorrelFactor * linearCorrelFactor) / (2.0 * N)
                 + boost::math::quantile(dist, 1 - alp/2) * (1 - linearCorrelFactor * linearCorrelFactor) / std::sqrt(N);
        }
        else
        {
            lw = std::tanh(
                0.5 * std::log((1.0 + linearCorrelFactor) / (1.0 - linearCorrelFactor))
                + 0.5 * linearCorrelFactor / (N - 1)
                - boost::math::quantile(dist, 1 - alp/2) / std::sqrt(N - 3));
            up = std::tanh(
                0.5 * std::log((1.0 + linearCorrelFactor) / (1.0 - linearCorrelFactor))
                + 0.5 * linearCorrelFactor / (N - 1)
                + boost::math::quantile(dist, 1 - alp/2) / std::sqrt(N - 3));
        }
        std::cout << std::setw(10) << alp << '\t' << std::setw(10) << lw << '\t' << std::setw(10) << up << "\n";
    }
    std::cout << std::endl;

    double ZsampleLinear   = linearCorrelFactor   * std::sqrt(N - 2) / std::sqrt(1 - linearCorrelFactor   * linearCorrelFactor  );
    double ZsampleSpearman = SpearmanCorrelFactor * std::sqrt(N - 2) / std::sqrt(1 - SpearmanCorrelFactor * SpearmanCorrelFactor);
    double ZsampleKendall  = std::sqrt((9.0 * N * (N - 1)) / (2 * (2.0 * N + 5))) * KendallCorrelFactor;

    students_t distT(N - 2);
    normal     distN(0, 1);

    double alpha = 0.1;

    double pvalLinear   = 2 * std::min(cdf(distT, ZsampleLinear  ), 1 - cdf(distT, ZsampleLinear  ));
    double pvalSpearman = 2 * std::min(cdf(distT, ZsampleSpearman), 1 - cdf(distT, ZsampleSpearman));
    double pvalKendall  = 2 * std::min(cdf(distN, ZsampleKendall ), 1 - cdf(distN, ZsampleKendall ));

    std::cout << std::setw(10) << "Type"     << '\t' << std::setw(10) << "Zsample"       << '\t' << std::setw(10) << "pvalue"     << '\t' << std::setw(10) << "Result\n";
    std::cout << std::setw(10) << "Linear"   << '\t' << std::setw(10) << ZsampleLinear   << '\t' << std::setw(10) << pvalLinear   << '\t' << std::setw(10) << "H0 is " << ((pvalLinear   > alpha) ? "true" : "false") << "\n";
    std::cout << std::setw(10) << "Spearman" << '\t' << std::setw(10) << ZsampleSpearman << '\t' << std::setw(10) << pvalSpearman << '\t' << std::setw(10) << "H0 is " << ((pvalSpearman > alpha) ? "true" : "false") << "\n";
    std::cout << std::setw(10) << "Kendall"  << '\t' << std::setw(10) << ZsampleKendall  << '\t' << std::setw(10) << pvalKendall  << '\t' << std::setw(10) << "H0 is " << ((pvalKendall  > alpha) ? "true" : "false") << "\n";
}

void calcKendall(const DataVec &data1, const DataVec &data2, double &factor, double &pval)
{
    DataInfo info1(data1),
             info2(data2);

    const int N = info1.N;

    DataVec ranks1, ranks2;
    calcRanks(data1, ranks1);
    calcRanks(data2, ranks2);

    DataInfo rankInfo1(ranks1),
             rankInfo2(ranks2);

    std::vector < std::pair< double, double > > rankPairs;
    for (int i = 0; i < N; ++i)
    {
        rankPairs.emplace_back(ranks1[i], ranks2[i]);
    }
    std::sort(rankPairs.begin(), rankPairs.end(),
        [] (const auto &a, const auto &b) -> bool
        {
            return a.first < b.first;
        }
    );
    int R = 0;
    for (int i = 0; i < N; ++i)
    {
        for (int j = i + 1; j < N; ++j)
        {
            if (rankPairs[i].second <= rankPairs[j].second)
                ++R;
        }
    }

    normal     distN(0, 1);

    double KendallCorrelFactor = 4.0 * R / (N * (N - 1)) - 1;
    double ZsampleKendall  = std::sqrt((9.0 * N * (N - 1)) / (2 * (2.0 * N + 5))) * KendallCorrelFactor;
    double pvalKendall  = 2 * std::min(cdf(distN, ZsampleKendall ), 1 - cdf(distN, ZsampleKendall ));

    factor = KendallCorrelFactor;
    pval   = pvalKendall;
}

void calc8_2(DataVec &data1, DataVec &data2, DataVec &data3)
{
    declareSection("#8.2");

    if (data1.size() != data2.size() || data2.size() != data3.size())
    {
        std::cout << "Wrong sizes\n";
        return;
    }
    const int N = data1.size();

    std::array< std::array< std::pair< double, double >, 3 >, 3 > KendallData;

    double tmpFactor, tmpPval;

    calcKendall(data1, data1, tmpFactor, tmpPval); KendallData[0][0] = std::make_pair(tmpFactor, tmpPval);
    calcKendall(data1, data2, tmpFactor, tmpPval); KendallData[0][1] = std::make_pair(tmpFactor, tmpPval);
    calcKendall(data1, data3, tmpFactor, tmpPval); KendallData[0][2] = std::make_pair(tmpFactor, tmpPval);

    calcKendall(data2, data1, tmpFactor, tmpPval); KendallData[1][0] = std::make_pair(tmpFactor, tmpPval);
    calcKendall(data2, data2, tmpFactor, tmpPval); KendallData[1][1] = std::make_pair(tmpFactor, tmpPval);
    calcKendall(data2, data3, tmpFactor, tmpPval); KendallData[1][2] = std::make_pair(tmpFactor, tmpPval);

    calcKendall(data3, data1, tmpFactor, tmpPval); KendallData[2][0] = std::make_pair(tmpFactor, tmpPval);
    calcKendall(data3, data2, tmpFactor, tmpPval); KendallData[2][1] = std::make_pair(tmpFactor, tmpPval);
    calcKendall(data3, data3, tmpFactor, tmpPval); KendallData[2][2] = std::make_pair(tmpFactor, tmpPval);

    std::cout << "Rang coeffs:\n";
    std::cout << std::setw(15) << "#\\#" << std::setw(15) << "#1" << std::setw(15) << "#2" << std::setw(15) << "#3" << std::endl;
    for (size_t i = 0; i < KendallData.size(); ++i)
    {
        std::cout << std::setw(15) << "#" + std::to_string(i+1);
        for (size_t j = 0; j < KendallData.size(); ++j)
        {
            std::cout << std::setw(15) << KendallData[i][j].first;
        }
        std::cout << std::endl;
    }
    std::cout << std::endl;

    std::cout << "Pvals:\n";
    std::cout << std::setw(15) << "#\\#" << std::setw(15) << "#1" << std::setw(15) << "#2" << std::setw(15) << "#3" << std::endl;
    for (size_t i = 0; i < KendallData.size(); ++i)
    {
        std::cout << std::setw(15) << "#" + std::to_string(i+1);
        for (size_t j = 0; j < KendallData.size(); ++j)
        {
            std::cout << std::setw(15) << KendallData[i][j].second;
        }
        std::cout << std::endl;
    }
    std::cout << std::endl;

    DataVec ranks1, ranks2, ranks3;
    calcRanks(data1, ranks1);
    calcRanks(data2, ranks2);
    calcRanks(data3, ranks3);

    double Rmean = 0;
    for (int i = 0; i < N; ++i)
    {
        Rmean += (ranks1[i] + ranks2[i] + ranks3[i]) / N;
    }

    double S = 0;
    for (int i = 0; i < N; ++i)
    {
        S += sqr(ranks1[i] + ranks2[i] + ranks3[i] - Rmean);
    }

    double W = 12.0 * S / (sqr(3 + 1) * (N*N*N - N));
    std::cout << "Concordance coeff = " << W << std::endl;
    std::cout << std::endl;

    double Zsample = 12.0 * S / ((3 + 1) * N * (N - 1));

    chi_squared dist(N - 1);

    double pval = 1 - cdf(dist, Zsample);

    std::vector<double> alphas;
    alphas.push_back(0.01);
    alphas.push_back(0.05);
    alphas.push_back(0.1);

    for (double alp: alphas)
    {
        printf("alpha = %f; Zsample = %f; pvalue = %f; H0 is %s\n", alp, Zsample, pval, (pval > alp) ? "true" : "false");
    }
}

void calc9_1(const DataVec &data1, const DataVec &data2)
{
    declareSection("#9.1");

    if (data1.size() != data2.size())
    {
        std::cout << "Wrong sizes\n";
        return;
    }
    const int N = data1.size();
    const double K = 2;

    DataInfo info1(data1),
             info2(data2);

    const double linearCorrelFactor = calcLinearCorrelFactor(
        data1,
        data2,
        info1.mean,
        info2.mean,
        info1.S,
        info2.S);

    const double beta0 = info2.mean - info1.mean * linearCorrelFactor * info2.S / info1.S;
    const double beta1 = linearCorrelFactor * info2.S / info1.S;

    std::cout << "9.1.1" << std::endl;
    std::cout << "a)" << std::endl;
    std::cout << "beta0 = " << beta0 << std::endl;
    std::cout << "beta1 = " << beta1 << std::endl;
    std::cout << std::endl;

    auto f = [beta0, beta1] (double x) -> double { return beta0 + x * beta1; };

    double Dregr = 0;
    for (double x: data1)
    {
        Dregr += sqr(f(x) - info2.mean) / N;
    }

    const double DregrUnbiased = N * Dregr / (K - 1);

    double Dresidual = 0;

    for (int i = 0; i < N; ++i)
    {
        Dresidual += sqr(data2[i] - f(data1[i])) / N;
    }

    const double DresidualUnbiased = N * Dresidual / (N - K);

    double Dy = 0;
    for (double y: data2)
    {
        Dy += sqr(y - info2.mean) / N;
    }
    const double DyUnbiased = N * Dy / (N - 1);

    double Dx = 0;
    for (double x: data1)
    {
        Dx += sqr(x - info1.mean) / N;
    }
    const double DxUnbiased = N * Dx / (N - 1);


    std::cout << "g)" << std::endl;
    std::cout << std::setw(15) << "Source"   << std::setw(15) << "Value"   << std::setw(15) << "FreeDegr" << std::setw(15) << "Unbiased"        << std::endl;
    std::cout << std::setw(15) << "Factor"   << std::setw(15) << Dregr     << std::setw(15) << K - 1      << std::setw(15) << DregrUnbiased     << std::endl;
    std::cout << std::setw(15) << "Residual" << std::setw(15) << Dresidual << std::setw(15) << N - K      << std::setw(15) << DresidualUnbiased << std::endl;
    std::cout << std::setw(15) << "All"      << std::setw(15) << Dy        << std::setw(15) << N - 1      << std::setw(15) << DyUnbiased        << std::endl;
    std::cout << std::endl;

    std::cout << "d)" << std::endl;
    std::cout << std::setw(15) << "Dregr"   << std::setw(15) << "Dresidual"   << std::setw(15) << "Dall" << std::setw(20) << "Dregr + Dresidual" << std::endl;
    std::cout << std::setw(15) << Dregr     << std::setw(15) << Dresidual     << std::setw(15) << Dy     << std::setw(20) << Dregr + Dresidual   << std::endl;
    std::cout << std::endl;


    const double determFactor = Dregr / Dy;
    const double correlRatio  = std::sqrt(determFactor);

    std::cout << "e)" << std::endl;
    std::cout << "Determination factor = " << determFactor << std::endl;
    std::cout << "Correlation   ratio  = " << correlRatio  << std::endl;
    std::cout << std::endl;



    std::vector<double> alphas;
    alphas.push_back(0.01);
    alphas.push_back(0.05);
    alphas.push_back(0.1);


    std::cout << "9.1.2" << std::endl;
    std::cout << "b)" << std::endl;

    std::cout << std::setw(15) << "Alpha" << std::setw(15) << "Beta0_lower"   << std::setw(15) << "Beta0_upper" << std::endl;
    for (double alp: alphas)
    {
        students_t dist(N - 2);

        double tmp = 0;
        for (double x: data1)
        {
            tmp += sqr(x / N);
        }

        const double b0diff = cdf(dist, 1 - alp / 2) * std::sqrt(DresidualUnbiased * tmp / Dx);
        const double beta0lw = beta0 - b0diff;
        const double beta0up = beta0 + b0diff;

        std::cout << std::setw(15) << alp << std::setw(15) << beta0lw   << std::setw(15) << beta0up << std::endl;
    }
    std::cout << std::endl;

    std::cout << std::setw(15) << "Alpha" << std::setw(15) << "Beta1_lower"   << std::setw(15) << "Beta1_upper" << std::endl;
    for (double alp: alphas)
    {
        students_t dist(N - 2);

        const double b1diff = cdf(dist, 1 - alp / 2) * std::sqrt(DresidualUnbiased / (N * Dx));
        const double beta1lw = beta1 - b1diff;
        const double beta1up = beta1 + b1diff;

        std::cout << std::setw(15) << alp << std::setw(15) << beta1lw   << std::setw(15) << beta1up << std::endl;
    }
    std::cout << std::endl;

    const double Zsample = (Dregr / (K - 1)) / (Dresidual / (N - K));

    fisher_f dist(K - 1, N - K);

    double pval = 2 * std::min(cdf(dist, Zsample), 1 - cdf(dist, Zsample));

    std::cout << "9.1.3" << std::endl;
    for (double alp: alphas)
    {
        printf("alpha = %f; Zsample = %f; pvalue = %f; H0 is %s\n", alp, Zsample, pval, (pval > alp) ? "true" : "false");
    }
}

template<class T>
bool invertMatrix(const matrix<T>& input, matrix<T>& inverse)
{
    typedef permutation_matrix<std::size_t> pmatrix;

    // create a working copy of the input
    matrix<T> A(input);

    // create a permutation matrix for the LU-factorization
    pmatrix pm(A.size1());

    // perform LU-factorization
    int res = lu_factorize(A, pm);
    if (res != 0)
        return false;

    // create identity matrix of "inverse"
    inverse.assign(identity_matrix<T> (A.size1()));

    // backsubstitute to get the inverse
    lu_substitute(A, pm, inverse);

    return true;
}

void calc9_2(const DataVec &data1, const DataVec &data2)
{
    declareSection("#9.2");

    if (data1.size() != data2.size())
    {
        std::cout << "Wrong sizes\n";
        return;
    }
    const int N = data1.size();
    const double K = 3;

    DataInfo info1(data1),
             info2(data2);

    matrix< double > Y(N, 1);
    matrix< double > beta(K, 1);
    matrix< double > FTF(K, K);
    matrix< double > invFTF(K, K);
    matrix< double > F(N, 3);
    matrix< double > FT;

    for (int i = 0; i < N; ++i)
    {
        Y(i, 0) = data2[i];

        F(i, 0) = 1;
        F(i, 1) = data1[i];
        F(i, 2) = sqr(data1[i]);
    }

    FT = trans(F);
    FTF = prod(FT, F);

    while (! invertMatrix(FTF, invFTF))
    {
        FTF(0, 0) += 0.001;
        FTF(1, 1) += 0.001;
        FTF(2, 2) += 0.001;
    }


    matrix< double > FTFinvFT = prod(invFTF, FT);
    beta = prod(FTFinvFT, Y);

    const double beta0 = beta(0, 0);
    const double beta1 = beta(1, 0);
    const double beta2 = beta(2, 0);

    std::cout << "9.2.1" << std::endl;
    std::cout << "a)" << std::endl;
    std::cout << "beta0 = " << beta0 << std::endl;
    std::cout << "beta1 = " << beta1 << std::endl;
    std::cout << "beta2 = " << beta2 << std::endl;
    std::cout << std::endl;


    auto f = [beta0, beta1, beta2] (double x) -> double { return beta0 + x * beta1 + x*x*beta2; };

    double Dregr = 0;
    for (double x: data1)
    {
        Dregr += sqr(f(x) - info2.mean) / N;
    }

    const double DregrUnbiased = N * Dregr / (K - 1);

    double Dresidual = 0;

    for (int i = 0; i < N; ++i)
    {
        Dresidual += sqr(data2[i] - f(data1[i])) / N;
    }

    const double DresidualUnbiased = N * Dresidual / (N - K);

    double Dy = 0;
    for (double y: data2)
    {
        Dy += sqr(y - info2.mean) / N;
    }
    const double DyUnbiased = N * Dy / (N - 1);

    double Dx = 0;
    for (double x: data1)
    {
        Dx += sqr(x - info1.mean) / N;
    }
    const double DxUnbiased = N * Dx / (N - 1);

    std::cout << "v)" << std::endl;
    std::cout << std::setw(15) << "Source"   << std::setw(15) << "Value"   << std::setw(15) << "FreeDegr" << std::setw(15) << "Unbiased"        << std::endl;
    std::cout << std::setw(15) << "Factor"   << std::setw(15) << Dregr     << std::setw(15) << K - 1      << std::setw(15) << DregrUnbiased     << std::endl;
    std::cout << std::setw(15) << "Residual" << std::setw(15) << Dresidual << std::setw(15) << N - K      << std::setw(15) << DresidualUnbiased << std::endl;
    std::cout << std::setw(15) << "All"      << std::setw(15) << Dy        << std::setw(15) << N - 1      << std::setw(15) << DyUnbiased        << std::endl;
    std::cout << std::endl;

    std::cout << "g)" << std::endl;
    std::cout << std::setw(15) << "Dregr"   << std::setw(15) << "Dresidual"   << std::setw(15) << "Dall" << std::setw(20) << "Dregr + Dresidual" << std::endl;
    std::cout << std::setw(15) << Dregr     << std::setw(15) << Dresidual     << std::setw(15) << Dy     << std::setw(20) << Dregr + Dresidual   << std::endl;
    std::cout << std::endl;

    const double determFactor = Dregr / Dy;
    const double correlRatio  = std::sqrt(determFactor);

    std::cout << "d)" << std::endl;
    std::cout << "Determination factor = " << determFactor << std::endl;
    std::cout << "Correlation   ratio  = " << correlRatio  << std::endl;
    std::cout << std::endl;


    const double Zsample = (Dregr / (K - 1)) / (Dresidual / (N - K));

    fisher_f dist(K - 1, N - K);

    double pval = 1 - cdf(dist, Zsample);

    std::vector<double> alphas;
    alphas.push_back(0.01);
    alphas.push_back(0.05);
    alphas.push_back(0.1);

    std::cout << "9.2.3" << std::endl;
    for (double alp: alphas)
    {
        printf("alpha = %f; Zsample = %f; pvalue = %f; H0 is %s\n", alp, Zsample, pval, (pval > alp) ? "true" : "false");
    }
}

void calc9_3(const DataVec &data1, const DataVec &data2, const DataVec &data3)
{
    declareSection("#9.3");

    if (data1.size() != data2.size() || data1.size() != data3.size())
    {
        std::cout << "Wrong sizes\n";
        return;
    }
    const int N = data1.size();
    const double K = 3;

    DataInfo info1(data1),
             info2(data2),
             info3(data3);

    matrix< double > Y(N, 1);
    matrix< double > beta(K, 1);
    matrix< double > FTF(K, K);
    matrix< double > invFTF(K, K);
    matrix< double > F(N, 3);
    matrix< double > FT;

    for (int i = 0; i < N; ++i)
    {
        Y(i, 0) = data3[i];

        F(i, 0) = 1;
        F(i, 1) = data1[i];
        F(i, 2) = data2[i];
    }

    FT = trans(F);
    FTF = prod(FT, F);

    while (! invertMatrix(FTF, invFTF))
    {
        FTF(0, 0) += 0.001;
        FTF(1, 1) += 0.001;
        FTF(2, 2) += 0.001;
    }


    matrix< double > FTFinvFT = prod(invFTF, FT);
    beta = prod(FTFinvFT, Y);

    const double beta0 = beta(0, 0);
    const double beta1 = beta(1, 0);
    const double beta2 = beta(2, 0);

    std::cout << "a)" << std::endl;
    std::cout << "beta0 = " << beta0 << std::endl;
    std::cout << "beta1 = " << beta1 << std::endl;
    std::cout << "beta2 = " << beta2 << std::endl;
    std::cout << std::endl;


    auto f = [beta0, beta1, beta2] (double x1, double x2) -> double { return beta0 + x1 * beta1 + x2 * beta2; };

    double Dregr = 0;
    for (int i = 0; i < N; ++i)
    {
        Dregr += sqr(f(data1[i], data2[i]) - info3.mean) / N;
    }

    const double DregrUnbiased = N * Dregr / (K - 1);

    double Dresidual = 0;

    for (int i = 0; i < N; ++i)
    {
        Dresidual += sqr(data3[i] - f(data1[i], data2[i])) / N;
    }

    const double DresidualUnbiased = N * Dresidual / (N - K);

    double Dy = 0;
    for (double y: data3)
    {
        Dy += sqr(y - info3.mean) / N;
    }
    const double DyUnbiased = N * Dy / (N - 1);

    std::cout << "v)" << std::endl;
    std::cout << std::setw(15) << "Source"   << std::setw(15) << "Value"   << std::setw(15) << "FreeDegr" << std::setw(15) << "Unbiased"        << std::endl;
    std::cout << std::setw(15) << "Factor"   << std::setw(15) << Dregr     << std::setw(15) << K - 1      << std::setw(15) << DregrUnbiased     << std::endl;
    std::cout << std::setw(15) << "Residual" << std::setw(15) << Dresidual << std::setw(15) << N - K      << std::setw(15) << DresidualUnbiased << std::endl;
    std::cout << std::setw(15) << "All"      << std::setw(15) << Dy        << std::setw(15) << N - 1      << std::setw(15) << DyUnbiased        << std::endl;
    std::cout << std::endl;

    std::cout << "g)" << std::endl;
    std::cout << std::setw(15) << "Dregr"   << std::setw(15) << "Dresidual"   << std::setw(15) << "Dall" << std::setw(20) << "Dregr + Dresidual" << std::endl;
    std::cout << std::setw(15) << Dregr     << std::setw(15) << Dresidual     << std::setw(15) << Dy     << std::setw(20) << Dregr + Dresidual   << std::endl;
    std::cout << std::endl;

    const double determFactor = Dregr / Dy;
    const double correlRatio  = std::sqrt(determFactor);

    std::cout << "d)" << std::endl;
    std::cout << "Determination factor = " << determFactor << std::endl;
    std::cout << "Correlation   ratio  = " << correlRatio  << std::endl;
    std::cout << std::endl;
}

void loadData(DataVec &data, std::string const &fileName)
{
    std::ifstream inFile(fileName);

    if (! inFile)
        throw std::string("File not found: " + fileName);

    std::string line;
    while (std::getline(inFile, line))
    {
        double d;
        try
        {
            d = std::stod(line);
        }
        catch (std::exception &e)
        {
            throw std::string("Error parsing float: " + std::string(e.what()));
        }
        data.push_back(d);
    }
}

void loadQualityData(QualityData &data, std::string const &fileName)
{
    std::ifstream inFile(fileName);

    if (! inFile)
        throw std::string("File not found: " + fileName);

    std::string line;
    while (std::getline(inFile, line))
        data.push_back(line);
}

#define check(s) \
if (!s)          \
    throw std::string("Not enough parameters in " + line);  \


void process(std::string const &confName)
{
    std::ifstream inFile(confName);

    std::string line;
    while (std::getline(inFile, line))
    {
        try
        {
            std::istringstream iss(line);
            check(iss);

            std::string ft;
            iss >> ft;

            if (ft == "printGraphData")
            {
                printGraphData = true;
            }
            if (ft == "useSturges")
            {
                intervalCalculator = intervalSturges;
            }
            else if (ft == "useScott")
            {
                intervalCalculator = intervalScott;
            }
            else if (ft == "1.2")
            {
                check(iss);

                std::string fileName;
                iss >> fileName;

                DataVec vec;
                loadData(vec, fileName);

                calc1_2(vec);
            }
            else if (ft == "2.1")
            {
                check(iss);

                std::string fileName;
                iss >> fileName;

                DataVec vec;
                loadData(vec, fileName);

                calc2_1(vec);
            }
            else if (ft == "2.2")
            {
                check(iss);

                std::string fileName;
                iss >> fileName;

                DataVec vec;
                loadData(vec, fileName);

                calc2_2(vec);
            }
            else if (ft == "2.3")
            {
                check(iss);

                std::string fileName1, fileName2;
                iss >> fileName1;
                check(iss);
                iss >> fileName2;

                DataVec vec1, vec2;
                loadData(vec1, fileName1);
                loadData(vec2, fileName2);

                calc2_3(vec1, vec2);
            }
            else if (ft == "2.4")
            {
                check(iss);

                std::string fileName1, fileName2;
                iss >> fileName1;
                check(iss);
                iss >> fileName2;

                DataVec vec1, vec2;
                loadData(vec1, fileName1);
                loadData(vec2, fileName2);

                calc2_4(vec1, vec2);
            }
            else if (ft == "3.1")
            {
                check(iss);

                std::string fileName;
                iss >> fileName;
                check(iss);

                std::string str_m0;
                iss >> str_m0;

                double m0;
                try
                {
                    m0 = std::stod(str_m0);
                }
                catch (...)
                {
                    throw std::string("Failed to parse float in: " + line);
                }


                DataVec vec;
                loadData(vec, fileName);

                calc3_1(vec, m0);
            }
            else if (ft == "3.2")
            {
                check(iss);

                std::string fileName;
                iss >> fileName;
                check(iss);

                std::string str_s0;
                iss >> str_s0;

                double s0;
                try
                {
                    s0 = std::stod(str_s0);
                }
                catch (...)
                {
                    throw std::string("Failed to parse float in: " + line);
                }


                DataVec vec;
                loadData(vec, fileName);

                calc3_2(vec, s0);
            }
            else if (ft == "3.3")
            {
                check(iss);
                std::string fileName1, fileName2;
                iss >> fileName1;
                check(iss);
                iss >> fileName2;

                DataVec vec1, vec2;
                loadData(vec1, fileName1);
                loadData(vec2, fileName2);

                calc3_3(vec1, vec2);
            }
            else if (ft == "3.4")
            {
                check(iss);
                std::string fileName1, fileName2;
                iss >> fileName1;
                check(iss);
                iss >> fileName2;

                DataVec vec1, vec2;
                loadData(vec1, fileName1);
                loadData(vec2, fileName2);

                calc3_4(vec1, vec2);
            }
            else if (ft == "4.1")
            {
                check(iss);

                std::string fileName;
                iss >> fileName;

                DataVec vec;
                loadData(vec, fileName);

                calc4_1(vec);
            }
            else if (ft == "4.2")
            {
                check(iss);

                std::string fileName;
                iss >> fileName;

                DataVec vec;
                loadData(vec, fileName);

                calc4_2(vec);
            }
            else if (ft == "5.1")
            {
                check(iss);

                std::string fileName1, fileName2;
                iss >> fileName1;
                check(iss);
                iss >> fileName2;

                DataVec vec1, vec2;
                loadData(vec1, fileName1);
                loadData(vec2, fileName2);

                calc5_1(vec1, vec2);
            }
            else if (ft == "5.2")
            {
                check(iss);

                std::string fileName1, fileName2;
                iss >> fileName1;
                check(iss);
                iss >> fileName2;

                DataVec vec1, vec2;
                loadData(vec1, fileName1);
                loadData(vec2, fileName2);

                calc5_2(vec1, vec2);
            }
            else if (ft == "6.1")
            {
                check(iss);

                std::string fileName1, fileName2;
                iss >> fileName1;
                check(iss);
                iss >> fileName2;

                QualityData vec1, vec2;
                loadQualityData(vec1, fileName1);
                loadQualityData(vec2, fileName2);

                calc6_1(vec1, vec2);
            }
            else if (ft == "7.1")
            {
                check(iss);

                std::string fileName1, fileName2;
                iss >> fileName1;
                check(iss);
                iss >> fileName2;

                QualityData vec1;
                DataVec     vec2;
                loadQualityData(vec1, fileName1);
                loadData(vec2, fileName2);

                calc7_1(vec1, vec2);
            }
            else if (ft == "8.1")
            {
                check(iss);

                std::string fileName1, fileName2;
                iss >> fileName1;
                check(iss);
                iss >> fileName2;

                DataVec vec1, vec2;
                loadData(vec1, fileName1);
                loadData(vec2, fileName2);

                calc8_1(vec1, vec2);
            }
            else if (ft == "8.2")
            {
                check(iss);
                std::string fileName1, fileName2, fileName3;
                iss >> fileName1;
                check(iss);
                iss >> fileName2;
                check(iss);
                iss >> fileName3;

                DataVec vec1, vec2, vec3;
                loadData(vec1, fileName1);
                loadData(vec2, fileName2);
                loadData(vec3, fileName3);

                calc8_2(vec1, vec2, vec3);
            }
            else if (ft == "9.1")
            {
                check(iss);

                std::string fileName1, fileName2;
                iss >> fileName1;
                check(iss);
                iss >> fileName2;

                DataVec vec1, vec2;
                loadData(vec1, fileName1);
                loadData(vec2, fileName2);

                calc9_1(vec1, vec2);
            }
            else if (ft == "9.2")
            {
                check(iss);

                std::string fileName1, fileName2;
                iss >> fileName1;
                check(iss);
                iss >> fileName2;

                DataVec vec1, vec2;
                loadData(vec1, fileName1);
                loadData(vec2, fileName2);

                calc9_2(vec1, vec2);
            }
            else if (ft == "9.3")
            {
                check(iss);

                std::string fileName1, fileName2, fileName3;
                iss >> fileName1;
                check(iss);
                iss >> fileName2;
                check(iss);
                iss >> fileName3;

                DataVec vec1, vec2, vec3;
                loadData(vec1, fileName1);
                loadData(vec2, fileName2);
                loadData(vec3, fileName3);

                calc9_3(vec1, vec2, vec3);
            }
        }
        catch (std::string &err)
        {
            std::cout << err << std::endl;
        }
    }
}

int main()
{
    std::cout.precision(10);

    process("config.txt");

    std::cout << "Finished" << std::endl;
    char c;
    std::cin >> c;

    return 0;
}

